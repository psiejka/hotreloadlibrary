#include <iostream>
#include "example.h"
#define OLC_PGE_APPLICATION
#include "olc/olcPixelGameEngine.h"

class OLC_Example : public olc::PixelGameEngine
{
public:
	OLC_Example()
	{
		sAppName = "HotReloading Example";
	}
public:
	bool OnUserCreate() override
	{
		test.LoadDynamicLibrary();
		test.Foo();
		return true;
	}
	bool OnUserUpdate(float fElapsedTime) override
	{
		for (int x = 0; x < ScreenWidth(); x++)
			for (int y = 0; y < ScreenHeight() / 2; y++)
				Draw(x, y, olc::Pixel(color, color, color));
		color += 2;

		for (int x = 0; x < ScreenWidth() / 2; x++)
			for (int y = 0; y < ScreenHeight() / 2; y++)
				Draw(x, y, olc::Pixel(test.GetBackgroundColor(), test.GetBackgroundColor(), test.GetBackgroundColor()));
		test.ReloadDynamicLibrary();
		return true;
	}
private:
	Example test;
	uint8_t color = 0;
};

int main()
{

	OLC_Example demo;
	if (demo.Construct(256, 240, 4, 4))
		demo.Start();

	return 0;
}
