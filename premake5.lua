workspace "HotReloading"
  configurations {"Debug", "Release"}
  configuration "linux"
	links { "dl", "stdc++fs", "png", "pthread", "GL", "GLU", "X11" }
  language "C++"

  targetdir "bin/%{cfg.buildcfg}"
  cppdialect "C++17"
  filter { "system:windows", "files:**_Linux.*"}
    flags { "ExcludeFromBuild"}
  filter { "system:linux", "files:**_Windows.*"}
    flags { "ExcludeFromBuild"}

  filter "configurations:Debug"
    defines { "DEBUG" }
    symbols "On"
	debugdir "bin/Debug"

  filter "configurations:Release"
    defines { "RELEASE" }
    optimize "On"
	debugdir "bin/Release"
	
  project "HotReload"
    kind "ConsoleApp"
    files { "lib/**.h", "lib/**.cpp", "runtime/**.h",  "runtime/**.cpp", "sharedLib/*.h", "sharedLib/**.cpp" }
	includedirs { "lib/", "lib/WindowsSpecific/", "lib/LinuxSpecific/", "sharedLib/", "runtime/"  }
	links { "HotReload_test" }

  project "HotReload_test"
    kind "SharedLib"
    files { "sharedLib/**.h", "sharedLib/**.cpp" }
    includedirs { "lib/", "lib/WindowsSpecific/", "lib/LinuxSpecific/", "sharedLib/", "runtime/olc" }
