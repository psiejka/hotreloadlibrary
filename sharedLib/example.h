#pragma once
#include "HotReloadLib.h"
#include <vector>

/*Main header of dynamic library. It provides methods and functions 
that will be reloaded in runtime.*/

#if _WIN32
#	define JIT_IMPL __declspec(dllexport)
#elif __unix
#	define JIT_IMPL  
#	define _DEBUG DEBUG
#endif

extern "C" JIT_IMPL void foo();
extern "C" JIT_IMPL int backgroundColor;

//Array of symbols and theirs addresses
//We use const char* because of Windows API.
//void* is a universal pointer
static std::vector<std::pair<const char*, void*>> g_exports {
  std::make_pair("foo", nullptr),
  std::make_pair("backgroundColor", nullptr),
};

//A class that provide informations about library and its functions
class Example : public HotReloadLib {
public:
	Example() : HotReloadLib{ g_exports } {}

	void Foo();
	int GetBackgroundColor();

protected:
	//Function with a name of dynamic library file
	virtual const char* GetFileName() const override {

		//ifdefs only for cross-platform testing
#ifdef _DEBUG
#ifdef _WIN32
		return "HotReload_test.dll";
#elif __unix__
		return "libHotReload_test.so";
#endif
#else
#ifdef _WIN32
		return "HotReload_test.dll";
#elif __unix__
		return "libHotReload_test.so";
#endif
#endif
	}
};
