#include "example.h"
#include <iostream>

void foo() {
	std::cout << "HELLO!\n";
}
int backgroundColor = 42;

void Example::Foo() {
	ExecuteFunction<0, decltype(&foo)>();
}

int Example::GetBackgroundColor() {
	return *GetVariable<1, decltype(backgroundColor)>();
}
