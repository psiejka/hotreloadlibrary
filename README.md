# HotReload Library
This is a cross-platform (Linux, Windows) library that provides ability to live code-reload that is very useful feature for development and software prototyping. The main goal was to create easy to use and reusable library that is a Proof of Concept of this idea.

### Building
There is a premake5.lua file that allows you to generate Makefile for your compiler or solution for your Visual Studio version. To do this you must have [premake5](https://premake.github.io/) installed on your machine.

For example, to generate files for Visual Studio 2017 invoke:

```sh
$ premake5 vs2017
```

### How to test
Compile this project, run executable file, change "backgroundColor" value and recompile. Tada! Magic. 

### External plugins
The example was written using [olcPixelGameEngine](https://github.com/OneLoneCoder/olcPixelGameEngine).

### TODOs:
* Create a FileWatcher class beacause now the library reloads code every frame.