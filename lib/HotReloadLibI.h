#pragma once

/*Interface for the Hot Reloading library*/
class HotReloadLibI {
public:
	//Loads the module into the address space
	virtual void Load(const char* fileName) = 0;
	//Reloads the module and addresses of functions
	virtual bool Reload(const char* fileName) = 0;
	//Frees the loaded dynamic-link library
	virtual void FreeLib() = 0;
	//Loads the address of functions from dynamic library to the 
	//variable m_symbols
	virtual void LoadSymbols() = 0;

	//virtual void RunFileWatcher() = 0;

};