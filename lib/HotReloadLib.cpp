#include "HotReloadLib.h"
#include <iostream>
#include <experimental/filesystem>

#if _WIN32
#	include <HotReloadLib_Windows.h>
#	define JIT_IMPL HotReloadLib_Windows
#elif __unix__
#	include <HotReloadLib_Linux.h>
#	define JIT_IMPL HotReloadLib_Linux
#endif

HotReloadLib::HotReloadLib(std::vector<std::pair<const char*, void*>>& symbols)
	: m_symbols(symbols) {
	m_pImpl = std::make_unique<JIT_IMPL>(symbols);
};

HotReloadLib::~HotReloadLib() {
}

void HotReloadLib::LoadDynamicLibrary() {
	m_pImpl->Load(GetPath().c_str());
}

void HotReloadLib::ReloadDynamicLibrary() {
	m_pImpl->Reload(GetPath().c_str());
}

const std::string HotReloadLib::GetPath() const {
	for (auto& p : std::experimental::filesystem::recursive_directory_iterator(std::experimental::filesystem::current_path())) {
		if (p.path().filename() == GetFileName()) {
			return (p.path().string());
		}
	}
	return {};
}