#include "HotReloadLib_Windows.h"

HotReloadLib_Windows::HotReloadLib_Windows(std::vector<std::pair<const char*, void*>>& symbols)
	: m_symbols{ symbols }
{
}

HotReloadLib_Windows::~HotReloadLib_Windows()
{
}

void HotReloadLib_Windows::Load(const char* filePath) {
	bool copied = false;
	while (!copied) {
		//it's a hack to bypass the lock on the original library file
		copied = CopyFileA(filePath, "HotReload_tmp.dll", FALSE);
	}
	m_libHandle = LoadLibraryA("HotReload_tmp.dll");
	LoadSymbols();
}

void HotReloadLib_Windows::LoadSymbols() {
	for (decltype(auto) symbol : m_symbols) {
		symbol.second = GetProcAddress(m_libHandle, symbol.first);
	}
}

bool HotReloadLib_Windows::Reload(const char* fileName) {
	FreeLibrary(m_libHandle);
	Load(fileName);
	return true;
}

void HotReloadLib_Windows::FreeLib() {
	FreeLibrary(m_libHandle);
}
