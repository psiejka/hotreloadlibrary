#pragma once
#include "HotReloadLib.h"
#include "HotReloadLibI.h"
#include <Windows.h>
#include <thread>
#include <mutex>
#include <atomic>

/*Implementation for Windows. It uses specialized functions and data 
types from <Windows.h> header.*/
class HotReloadLib_Windows :	public HotReloadLibI
{
public:
	HotReloadLib_Windows(std::vector<std::pair<const char*, void*>>& symbols);
	//Because of the Rule of Five
	HotReloadLib_Windows(HotReloadLib_Windows& hotReloadLib) = delete;
	HotReloadLib_Windows& operator=(HotReloadLib_Windows& hotReloadLib) = delete;
	HotReloadLib_Windows(HotReloadLib_Windows&& hotReloadLib) = delete;
	HotReloadLib_Windows& operator=(HotReloadLib_Windows&& hotReloadLib) = delete;
	virtual ~HotReloadLib_Windows();

protected:
	
	//Loads the module into the address space
	//This method uses a "hack" to bypass the lock on the 
	//original library file by copying the file
	virtual void Load(const char* filePath) override final;

	//Loads the address of functions from dynamic library to the 
	//variable m_symbols
	virtual void LoadSymbols() override final;

	//Reloads the module and addresses of functions
	virtual bool Reload(const char* filePath) override final;

	//Frees the loaded dynamic-link library
	virtual void FreeLib() override final;
	//virtual void RunFileWatcher() override final;
private:
	//Handle to the module
	HINSTANCE m_libHandle;

	//Array of symbols and their adresses
	std::vector<std::pair<const char*, void*>>& m_symbols;
};
