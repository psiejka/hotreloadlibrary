#pragma once
#include "HotReloadLib.h"
#include "HotReloadLibI.h"

/*Implementation for Linux. It uses specialized functions from <dlfcn.h> header.*/
class HotReloadLib_Linux : public HotReloadLibI
{
public:
	HotReloadLib_Linux(std::vector<std::pair<const char*, void*>>& symbols);
	//Because of the Rule of Five
	HotReloadLib_Linux(HotReloadLib_Linux& hotReloadLib) = delete;
	HotReloadLib_Linux& operator=(HotReloadLib_Linux& hotReloadLib) = delete;
	HotReloadLib_Linux(HotReloadLib_Linux&& hotReloadLib) = delete;
	HotReloadLib_Linux& operator=(HotReloadLib_Linux&& hotReloadLib) = delete;
	virtual ~HotReloadLib_Linux();

protected:
	//Loads the module into the address space
	virtual void Load(const char* filePath) override final;
	
	//Loads the address of functions from dynamic library to the 
	//variable m_symbols
	virtual void LoadSymbols() override final;
	
	//Reloads the module and addresses of functions
	virtual bool Reload(const char* filePath) override final;

	//Frees the loaded dynamic-link library
	virtual void FreeLib() override final;

	//virtual void RunFileWatcher() override final;

private:
	//Handle to the module
	void* m_libHandle;

	//Array of symbols and their adresses
	std::vector<std::pair<const char*, void*>>& m_symbols;
};
