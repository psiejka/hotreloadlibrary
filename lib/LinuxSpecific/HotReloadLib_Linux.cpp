#include "HotReloadLib_Linux.h"
#include <dlfcn.h>
#include <fstream>
#include <experimental/filesystem>

HotReloadLib_Linux::HotReloadLib_Linux(std::vector<std::pair<const char*, void*>>& symbols)
	: m_symbols{ symbols }
{
}

HotReloadLib_Linux::~HotReloadLib_Linux()
{
}

void HotReloadLib_Linux::Load(const char* filePath) {
	while (!m_libHandle) {
		m_libHandle = dlopen(filePath, RTLD_NOW);
	}
	LoadSymbols();
}
void HotReloadLib_Linux::LoadSymbols() {
	for (decltype(auto) symbol : m_symbols) {
		symbol.second = dlsym(m_libHandle, symbol.first);
	}
}
bool HotReloadLib_Linux::Reload(const char* filePath) {
	FreeLib();
	Load(filePath);
	return true;
}
void HotReloadLib_Linux::FreeLib() {
	dlclose(m_libHandle);
	m_libHandle = nullptr;
}