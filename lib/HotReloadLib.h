#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "HotReloadLibI.h"

/*Main header for this library. Declares all implementation classes
to ease the use of this library.*/

class HotReloadLib{
public:
	//Loads Dynamic Library
	void LoadDynamicLibrary();

	//Reloads Dynamic Library
	void ReloadDynamicLibrary();
	
protected:
	HotReloadLib(std::vector<std::pair<const char*, void*>>& symbols);
	//Because of the Rule of Five
	HotReloadLib(HotReloadLib& hotReloadLib) = delete;
	HotReloadLib& operator=(HotReloadLib& hotReloadLib) = delete;
	HotReloadLib(HotReloadLib&& hotReloadLib) = delete;
	HotReloadLib& operator=(HotReloadLib&& hotReloadLib) = delete;


	virtual ~HotReloadLib();

	//Lookups the symbol index, call it as a function with 
	//arguments and then returns the return value
	template<size_t index, typename ReturnType, typename... Args>
	ReturnType ExecuteFunction(Args... args) {
		if (index < m_symbols.size()) {
			auto symbol = m_symbols.at(index);
			return reinterpret_cast<ReturnType(*)(Args...)>(symbol.second)(args...);
		}
		else {
			throw std::runtime_error("Function not found!");
		}
	}

	//Searches for the symbol index and then cast it as a pointer
	//to the template type
	template<size_t index, typename U>
	U* GetVariable() {
		if (index < m_symbols.size()) {
			auto symbol = m_symbols.at(index);
			return static_cast<U*>(symbol.second);
		}
		else {
			throw std::runtime_error("Index out of bound.");
		}
	}
	
	//A method that provide information about path of the dynamic library file
	virtual const std::string GetPath() const;

	//A method that provide information about name of the dynamic library file
	virtual const char* GetFileName() const = 0;

private:
	//Array of symbols and theirs adresses
	std::vector<std::pair<const char*, void*>>& m_symbols;

	//The implementation of Hot Reloading
	std::unique_ptr<HotReloadLibI> m_pImpl;
};